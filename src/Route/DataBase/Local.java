package Route.DataBase;

import javax.persistence.Entity;

@Entity
public enum Local {
    PRAIAVERMELHA, VALONGUINHO, GRAGOATA;
}
