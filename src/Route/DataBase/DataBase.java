package Route.DataBase;

import Route.Location.CartesianStructs.CartesianAnchor;
import Route.Location.CartesianStructs.CartesianPoint;
import Route.Util.Graph;
import org.tmatesoft.sqljet.core.SqlJetTransactionMode;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;
import org.tmatesoft.sqljet.core.table.ISqlJetTable;
import org.tmatesoft.sqljet.core.table.SqlJetDb;

import java.io.File;

/**
 * Created by Mateus on 08/10/2014.
 */
public class DataBase {

    private final String DATABASE_PATH;

    public DataBase(Local local) {
        this.DATABASE_PATH = "$database/" + local.name() + ".odb";
    }

    public Graph<CartesianPoint, Double> retrieve() {
        Graph<CartesianPoint, Double> g = new Graph<>();
        try {
            SqlJetDb db = SqlJetDb.open(new File("PRAIAVERMELHA.db"), true);
            db.beginTransaction(SqlJetTransactionMode.WRITE);
            ISqlJetTable table = db.getTable("CARTESIAN_POINTS_CONNECTIONS");
            ISqlJetCursor cursor = table.open();
            if (!cursor.eof()) {
                do {
                    CartesianPoint a = new CartesianAnchor(cursor.getFloat(0), cursor.getFloat(1)), b = new CartesianAnchor(cursor.getFloat(2), cursor.getFloat(3));
                    if (!g.hasNode(a)) g.addNode(a);
                    if (!g.hasNode(b)) g.addNode(b);
                    g.addEdge(a, b, cursor.getFloat(4));
                } while (cursor.next());
            }
            cursor.close();
            db.commit();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return g;
    }
}
