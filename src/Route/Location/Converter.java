package Route.Location;

import Route.Location.CartesianStructs.CartesianAnchor;
import Route.Location.CartesianStructs.CartesianPoint;
import Route.Location.GeodesicStructs.GeodesicAnchor;
import Route.Location.GeodesicStructs.GeodesicPoint;

/**
 * Created by mateus on 03/12/14.
 */
public class Converter {
    private static final double RADIUS = 6360203.501128927;
    private static GeodesicPoint anchorG = new GeodesicAnchor(-43.1324688, -22.9053754);
    private static CartesianPoint anchorA = new CartesianAnchor(558, 538);
    private static double scale = 279.25615481131297 / 156.3843334209287;
    private static double offset = Math.toRadians(88.5);

    public GeodesicPoint toGeodesicPoint(CartesianPoint a) {
        double d = Math.sqrt(Math.pow(a.getCoordinateX() - anchorA.getCoordinateX(), 2) + Math.pow(a.getCoordinateY() - anchorA.getCoordinateY(), 2));
        d *= scale;
        return null;
    }

    public CartesianPoint toCartesianPoint(GeodesicPoint a) {
        double b = Math.pow(Math.sin((a.getLatitude() - anchorG.getLatitude()) / 2), 2) + Math.cos(a.getLatitude()) * Math.cos(anchorG.getLatitude()) * (Math.pow(Math.sin((a.getLongitude() - anchorG.getLongitude()) / 2), 2));
        double c = 2 * Math.atan2(Math.sqrt(b), Math.sqrt(1 - b));
        double d = c * RADIUS * scale;

        double theta = Math.atan2(a.getLongitude() - anchorG.getLongitude(), Math.log(Math.tan(Math.PI / 4 + a.getLatitude() / 2) / Math.tan(Math.PI / 4 + anchorG.getLatitude() / 2)));
        double x = d * Math.cos(theta), y = -d * Math.sin(theta);

        return new CartesianAnchor(anchorA.getCoordinateX() + (x * Math.cos(offset) - y * Math.sin(offset)), anchorA.getCoordinateY() - (x * Math.sin(offset) + y * Math.cos(offset)));
    }
}
