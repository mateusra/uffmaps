package Route.Location;

import Route.DataBase.DataBase;
import Route.DataBase.Local;
import Route.Location.CartesianStructs.CartesianPoint;
import Route.PathFinder.Path;
import Route.PathFinder.PathFinder;
import Route.Util.Graph;

import java.util.Iterator;

/**
 * Created by mateus on 04/12/14.
 */
public class Map {
    private Graph<CartesianPoint, Double> paths;

    public Map(Local local) {
        this.paths = (new DataBase(local)).retrieve();
    }

    public Path<CartesianPoint> findAWay(CartesianPoint from, CartesianPoint to) {
        if (!paths.hasNode(from)) {
            Iterator<CartesianPoint> pointIterator = paths.nodeIterator();
            CartesianPoint a = pointIterator.next();
            double d = Math.sqrt(Math.pow(a.getCoordinateX() - from.getCoordinateX(), 2) + Math.pow(a.getCoordinateY() - from.getCoordinateY(), 2));
            while (pointIterator.hasNext()) {
                CartesianPoint b = pointIterator.next();
                double d2 =
                        Math.sqrt(Math.pow(b.getCoordinateX() - from.getCoordinateX(), 2) + Math.pow(b.getCoordinateY() - from.getCoordinateY(), 2));
                if (d > d2) {
                    a = b;
                    d = d2;
                }
            }
            paths.addNode(from);
            paths.addEdge(from, a, d);
            paths.addEdge(a, from, d);
        }

        if (!paths.hasNode(to)) {
            Iterator<CartesianPoint> pointIterator = paths.nodeIterator();
            CartesianPoint a = pointIterator.next();
            double d = Math.sqrt(Math.pow(a.getCoordinateX() - to.getCoordinateX(), 2) + Math.pow(a.getCoordinateY() - to.getCoordinateY(), 2));
            while (pointIterator.hasNext()) {
                CartesianPoint b = pointIterator.next();
                double d2 = Math.sqrt(Math.pow(b.getCoordinateX() - to.getCoordinateX(), 2) + Math.pow(b.getCoordinateY() - to.getCoordinateY(), 2));
                if (d > d2) {
                    a = b;
                    d = d2;
                }
            }
            paths.addNode(to);
            paths.addEdge(to, a, d);
            paths.addEdge(a, to, d);
        }
        PathFinder<CartesianPoint> pathFinder = new PathFinder<>(paths);
        return pathFinder.findAWay(from, to);
    }
}
