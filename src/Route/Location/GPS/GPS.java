package Route.Location.GPS;

/**
 * Created by Mateus on 08/10/2014.
 */
public interface GPS {
    public double getLongitude();

    public double getLatitude();

    public double getAccuracy();

    public void locate();

    public boolean isReady();
}
