package Route.Location.GPS;

/**
 * Created by Mateus on 08/10/2014.
 */
public enum GPSAndroid implements Route.Location.GPS.GPS {
    //Constantes no Lugar do 0, 1, 2
    WIFI(0), GSM(1), GPS(2);
    //Tipo da Ferramenta de GPS
    private Object gps;

    GPSAndroid(int tipoGPS) {
        //Logica para criar o GPS pelo tipo da constante
        this.gps = tipoGPS;
    }

    @Override
    public double getLongitude() {
        return 0;
    }

    @Override
    public double getLatitude() {
        return 0;
    }

    @Override
    public double getAccuracy() {
        return 0;
    }

    @Override
    public void locate() {

    }

    @Override
    public boolean isReady() {
        return false;
    }
}
