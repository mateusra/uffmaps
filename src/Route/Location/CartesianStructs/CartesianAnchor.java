package Route.Location.CartesianStructs;

import java.io.Serializable;

public class CartesianAnchor extends CartesianPoint implements Serializable {
    private long id;
    public CartesianAnchor(double coordinateX, double coordinateY) {
        super(coordinateX, coordinateY);
    }

}
