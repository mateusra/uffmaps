package Route.Location.CartesianStructs;

import Route.DataBase.Local;
import Route.Location.GeodesicStructs.GeodesicPoint;
public abstract class CartesianPoint {

    private double coordinateX, coordinateY;

    public CartesianPoint(double coordinateX, double coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public double getCoordinateY() {
        return this.coordinateY;
    }

    public double getCoordinateX() {
        return this.coordinateX;
    }

    public GeodesicPoint convert(Local local) {
        throw new RuntimeException();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartesianPoint)) return false;

        CartesianPoint that = (CartesianPoint) o;

        if (Double.compare(that.coordinateX, coordinateX) != 0) return false;
        if (Double.compare(that.coordinateY, coordinateY) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(coordinateX);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(coordinateY);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public String toString() {
        return "{" + coordinateX + "," + coordinateY + "}";
    }
}
