package Route.Location.GeodesicStructs;

import Route.DataBase.Local;
import Route.Location.CartesianStructs.CartesianPoint;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public abstract class GeodesicPoint {
    @Id
    @GeneratedValue
    private long id;
    private double longitude, latitude;

    public GeodesicPoint(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public double getLatitude() {
        return Math.toRadians(this.latitude);
    }

    public double getLongitude() {
        return Math.toRadians(this.longitude);
    }

    public CartesianPoint convert(Local local) {
        throw new RuntimeException();
    }
}
