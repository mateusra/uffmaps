package Route.Location.GeodesicStructs;

import javax.persistence.Entity;

@Entity
public class GeodesicAnchor extends GeodesicPoint {
    public GeodesicAnchor(double longitude, double latitude) {
        super(longitude, latitude);
    }
}
