package Route.Util;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Edge<N, E> {
    @Id
    @GeneratedValue
    private long id;
    private N from, to;
    private E weight;

    public Edge(N from, N to, E weight) {
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public N getFrom() {
        return from;
    }

    public N getTo() {
        return to;
    }


    public E getWeight() {
        return weight;
    }

    public void setWeight(E weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;

        if (from != null ? !from.equals(edge.from) : edge.from != null) return false;
        if (to != null ? !to.equals(edge.to) : edge.to != null) return false;
        if (weight != null ? !weight.equals(edge.weight) : edge.weight != null) return false;

        return true;
    }


    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (weight != null ? weight.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Edge{" +
               "id=" + id +
               ", from=" + from +
               ", to=" + to +
               ", weight=" + weight +
               '}';
    }
}

