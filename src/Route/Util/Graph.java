package Route.Util;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.*;

@Entity
public class Graph<N, E> {

    @Id
    @GeneratedValue
    private long id;
    private java.util.Map<N, Set<Edge<N, E>>> graph;

    public Graph() {
        this.graph = new HashMap<>();
    }

    public void addNode(N node) {
        this.graph.put(node, new HashSet<>());
    }

    public void removeNode(N node) {
        for (Edge<N, E> edge : graph.get(node)) {
            this.removeEdge(edge.getTo(), edge.getFrom());
        }
        this.graph.remove(node);
    }

    public void addEdge(N from, N to, E weight) {
        this.graph.get(from).add(new Edge<N, E>(from, to, weight));
    }

    public void removeEdge(N from, N to) {
        for (Edge<N, E> edge : graph.get(from)) {
            if (edge.getTo().equals(to)) {
                graph.get(from).remove(edge);
                break;
            }
        }
    }

    public boolean hasNode(N node) {
        return this.graph.containsKey(node);
    }

    public boolean hasEdge(N from, N to) {
        for (Edge<N, E> edge : this.graph.get(from)) {
            if (edge.getTo().equals(to)) return true;
        }
        return false;
    }

    public boolean hasEdge(N from, N to, E weight) {
        return this.graph.get(from).contains(new Edge<N, E>(from, to, weight));
    }

    public Iterator<N> nodeIterator() {
        return this.graph.keySet().iterator();
    }

    public Iterator<Edge<N, E>> edgeIterator(N node) {
        return this.graph.get(node).iterator();
    }

    public String toString() {
        String message = "";
        for (Map.Entry<N, Set<Edge<N, E>>> entry : graph.entrySet()) {
            message += "[" + entry.getKey() + ":";
            for (Edge<N, E> edge : entry.getValue()) {
                message += edge.getTo() + "-" + edge.getWeight() + ",";
            }
            if (message.endsWith(",")) message = message.substring(0, message.lastIndexOf(","));
            message += "]\n";
        }
        return message;
    }
}
