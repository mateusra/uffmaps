package Route.PathFinder;

import Route.Util.Edge;
import Route.Util.Graph;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Iterator;

public class PathFinder<T> {

    private Graph<T, Double> paths;

    public PathFinder(Graph<T, Double> paths) {
        this.paths = paths;
    }

    private Path findAWay(Deque<T> pastNodes, T from, T to) {
        Path<T> minimal = null;
        if (this.paths.hasEdge(from, to)) {
            Iterator<Edge<T, Double>> edgeIterator = this.paths.edgeIterator(from);
            while (edgeIterator.hasNext()) {
                Edge<T, Double> edge = edgeIterator.next();
                if (edge.getTo().equals(to)) {
                    minimal = new Path<T>(Arrays.asList(edge), edge.getWeight());
                    break;
                }
            }
        } else {
            Iterator<Edge<T, Double>> edgeIterator = this.paths.edgeIterator(from);
            pastNodes.push(from);
            minimal = new Path<T>(Arrays.asList(new Edge<T, Double>(from, to, Double.MAX_VALUE)), Double.MAX_VALUE);
            while (edgeIterator.hasNext()) {
                Edge<T, Double> edge = edgeIterator.next();
                if (!pastNodes.contains(edge.getTo())) {
                    Path<T> possibleMinimal = this.findAWay(pastNodes, edge.getTo(), to);
                    possibleMinimal.addStep(edge, edge.getWeight());
                    if (minimal.getWeight() > possibleMinimal.getWeight()) {
                        minimal = possibleMinimal;
                    }
                }
            }
            pastNodes.pop();
        }
        return minimal;
    }

    public Path findAWay(T from, T to) {
        Path<T> p = this.findAWay(new ArrayDeque<T>(), to, from);
        return p;
    }
}
