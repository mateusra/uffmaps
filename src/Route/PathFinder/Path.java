package Route.PathFinder;

import Route.Util.Edge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Mateus on 08/10/2014.
 */
public class Path<T> {
    private List<Edge<T, Double>> steps;
    private double weight;

    public Path(Edge<T, Double> first) {
        this(Arrays.asList(first), 0.0);
    }

    public Path(List<Edge<T, Double>> steps, double weight) {
        this.steps = new ArrayList<>(steps);
        this.weight = weight;
    }

    public List<Edge<T, Double>> getSteps() {
        return this.steps;
    }

    public boolean contains(T point) {
        return this.steps.contains(point);
    }

    public double getWeight() {
        return this.weight;
    }

    public void addStep(Edge<T, Double> step, double weight) {
        if (!this.steps.contains(step)) {
            this.steps.add(step);
            this.weight += weight;
        }
    }

    public Path<T> clone() {
        return new Path<T>(new ArrayList<>(this.steps), this.weight);
    }

    @Override
    public String toString() {
        return "Path{" +
               "steps=" + steps +
               ", weight=" + weight +
               '}';
    }
}
