import Route.DataBase.Local;
import Route.Location.CartesianStructs.CartesianAnchor;
import Route.Location.CartesianStructs.CartesianPoint;
import Route.Location.Map;
import Route.PathFinder.Path;
import Route.PathFinder.PathFinder;
import Route.Util.Edge;
import Route.Util.Graph;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class PathMaker {
    Path<CartesianPoint> p;
    /**
     * The texture that will hold the image details
     */
    private Texture texture;

    /**
     * Main Class
     */
    public static void main(String[] argv) throws LWJGLException {
        PathMaker textureExample = new PathMaker();
        textureExample.start();

        System.out.println(Math.sqrt(Math.pow(780 - 560, 2) + Math.pow(367 - 539, 2)));

    }

    /**
     * Start the example
     */

    private boolean noone() {
        for (int i = 0; i < Mouse.getButtonCount(); i++) {
            if (Mouse.isButtonDown(i)) return false;
        }
        return true;
    }

    public void start() throws LWJGLException {
        initGL(1000, 1146);
        init();
        Mouse.create();
        Keyboard.create();
        Keyboard.enableRepeatEvents(false);
        Graph<CartesianPoint, Double> g = Miner.make();
        CartesianPoint selectA = null, selectB = null;
        boolean press = false, key = false;
        PathFinder<CartesianPoint> pathFinder = new PathFinder<>(g);
        CartesianPoint x = new CartesianAnchor(971.0, 238.0), y = new CartesianAnchor(436.0, 530.0);
        p = pathFinder.findAWay(x, y);
        while (true) {
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
            render();
            if (Mouse.isButtonDown(0) && !press) {
                press = true;
                CartesianPoint h = new CartesianAnchor(Mouse.getX(), 1146 - Mouse.getY());
                if (!g.hasNode(h)) g.addNode(h);
            } else if (Mouse.isButtonDown(1) && !press) {
                press = true;
                CartesianPoint h = new CartesianAnchor(Mouse.getX(), 1146 - Mouse.getY());
                Iterator<CartesianPoint> iterator = g.nodeIterator();
                while (iterator.hasNext()) {
                    CartesianPoint a = iterator.next();
                    if (Math.sqrt(Math.pow(h.getCoordinateX() - a.getCoordinateX(), 2) + Math.pow(h.getCoordinateY() - a.getCoordinateY(), 2)) < 7) {
                        selectB = selectA;
                        selectA = a;
                        System.out.println(selectA);
                        break;
                    }
                }
            } else if (noone()) {
                press = false;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_RETURN)) {
                if (selectA != null && selectB != null && !g.hasEdge(selectA, selectB)) {
                    g.addEdge(selectA, selectB, 0.0);
                    g.addEdge(selectB, selectA, 0.0);
                    selectA = selectB = null;
                }
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_DELETE)) {
                if (selectA != null && g.hasNode(selectA)) {
                    g.removeNode(selectA);
                }
                if (selectB != null && g.hasNode(selectB)) {
                    g.removeNode(selectB);
                }
                selectA = selectB = null;
            }
            if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
                key = true;
                selectA = selectB = null;
            }

            if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
                if (selectA != null && selectB != null) {
                    Map m = new Map(Local.PRAIAVERMELHA);
                    p = m.findAWay(selectA, selectB);
                }
                selectA = selectB = null;
            }
            draw(g, selectA, selectB);
            Display.update();
            Display.sync(100);

            if (Display.isCloseRequested()) {
                Display.destroy();
                System.out.println(g);
                System.exit(0);
            }

        }
    }


    private void draw(Graph<CartesianPoint, Double> g, CartesianPoint a, CartesianPoint b) {
        Deque<CartesianPoint> pastDraw = new ArrayDeque<>();
        Iterator<CartesianPoint> pointIterator = g.nodeIterator();
        GL11.glLineWidth(5);
        GL11.glBegin(GL11.GL_LINES);
        while (pointIterator.hasNext()) {
            CartesianPoint c = pointIterator.next();
            Iterator<Edge<CartesianPoint, Double>> edgeIterator = g.edgeIterator(c);
            while (edgeIterator.hasNext()) {
                Edge<CartesianPoint, Double> edge = edgeIterator.next();
                if (!pastDraw.contains(edge.getTo())) {
                    GL11.glColor3f(1, 0, 0);
                    GL11.glVertex2d(edge.getFrom().getCoordinateX(), edge.getFrom().getCoordinateY());
                    GL11.glColor3f(1, 0, 0);
                    GL11.glColor3f(1, 0, 0);
                    GL11.glVertex2d(edge.getTo().getCoordinateX(), edge.getTo().getCoordinateY());
                    GL11.glColor3f(1, 0, 0);
                }
            }
            pastDraw.add(c);
        }
        GL11.glColor3f(0, 0, 1);
        for (Edge<CartesianPoint, Double> edge : p.getSteps()) {
            GL11.glColor3f(0, 0, 1);
            GL11.glVertex2d(edge.getFrom().getCoordinateX(), edge.getFrom().getCoordinateY());
            GL11.glColor3f(0, 0, 1);
            GL11.glColor3f(0, 0, 1);
            GL11.glVertex2d(edge.getTo().getCoordinateX(), edge.getTo().getCoordinateY());
            GL11.glColor3f(0, 0, 1);
        }
        GL11.glEnd();

        GL11.glPointSize(5);
        GL11.glBegin(GL11.GL_POINTS);
        pointIterator = g.nodeIterator();
        while (pointIterator.hasNext()) {
            CartesianPoint c = pointIterator.next();
            GL11.glColor3f(0, 0, 1);
            GL11.glVertex2d(c.getCoordinateX(), c.getCoordinateY());
            GL11.glColor3f(0, 0, 1);
        }
        GL11.glEnd();

        GL11.glPointSize(10);
        GL11.glBegin(GL11.GL_POINTS);

        GL11.glColor3f(0, 1, 0);
        if (a != null) GL11.glVertex2d(a.getCoordinateX(), a.getCoordinateY());
        GL11.glColor3f(0, 1, 0);
        GL11.glColor3f(0, 1, 0);
        if (b != null) GL11.glVertex2d(b.getCoordinateX(), b.getCoordinateY());
        GL11.glColor3f(0, 1, 0);
        GL11.glEnd();
    }

    /**
     * Initialise the GL display
     *
     * @param width  The width of the display
     * @param height The height of the display
     */

    private void initGL(int width, int height) {
        try {
            Display.setDisplayMode(new DisplayMode(width, height));
            Display.create();
            Display.setVSyncEnabled(true);
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        GL11.glEnable(GL11.GL_TEXTURE_2D);

        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        // enable alpha blending
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        GL11.glViewport(0, 0, width, height);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, width, height, 0, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
    }

    /**
     * Initialise resources
     */
    public void init() {

        try {
            // load texture from PNG file
            texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream("mapa.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * draw a quad with the image on it
     */
    public void render() {
        Color.white.bind();
        texture.bind(); // or GL11.glBind(texture.getTextureID());
        GL11.glBegin(GL11.GL_QUADS);
        GL11.glTexCoord2f(0, 0);
        GL11.glVertex2f(0, 0);
        GL11.glTexCoord2f(1, 0);
        GL11.glVertex2f(0 + texture.getTextureWidth(), 0);
        GL11.glTexCoord2f(1, 1);
        GL11.glVertex2f(0 + texture.getTextureWidth(), 0 + texture.getTextureHeight());
        GL11.glTexCoord2f(0, 1);
        GL11.glVertex2f(0, 0 + texture.getTextureHeight());
        GL11.glEnd();
    }

    static class Point {
        double x, y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (Double.compare(point.x, x) != 0) return false;
            if (Double.compare(point.y, y) != 0) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            temp = Double.doubleToLongBits(x);
            result = (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(y);
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public String toString() {
            return x + ", " + y;
        }
    }
}

